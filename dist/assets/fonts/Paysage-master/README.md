# Paysage

*Paysage is a sans-serif font by Anton Moglia (http://www.maous.fr/).*

![specimen](/documentation/images/1.jpg?raw=true "Paysage")

# ENGLISH VERSION

*Paysage is a humanist-style sans-serif typeface. It's characterised by a distinctive "g" and a regular rhythm. Paysage has been designed to give a feeling of simplicity and tranquility to the reader. Its design is adapted to the composition of short or long texts, legends or annotations. Paysage has been inspired by Roger Excoffon's Antique Olive, but it's also imbued with other classics such as Otl Aicher's Rotis Sans or Matthew Carter's Verdana. The character set provides linguistic support for most European languages based on Latin. Paysage is a redesigned and extended version of Garcia Regular, a typeface started in 2016.*

## Paysage's story

This typeface is above all a stylistic exercise and it has evolved with my practice: I started designing it at the end of 2016 under the name Garcia, when I began to learn type design. Back then, Garcia Regular had many youthful indiscretions but it also had already had a certain personality, with a characteristic g.
Over time, it has been used a few times, especially in printed media and in the fanzine world, which pushed me to update it in 2018 with some minor improvements.
In 2019, I developed a version with several letter widths for the magazine Paysageur, which made use of it. This rather extravagant version remained confidential, as I wasn't proud enough of its shapes as to make it public. However, this experience helped me clarify Garcia's drawing. I was thus able to explore new directions and better define what I wanted to do in order to improve it.

In 2020, the Tunera Type Foundry started using the Garcia font, which encouraged me to develop at least one more weight from its original version. I proposed the foundry to distribute the typeface to give it more visibility, and I decided to propose a more sober version adapted to the interface of their website.  I then renamed the typeface "Paysage" (french for Landscape). First of all, this is a nod to its story within the magazine Paysageur, but also a reference to my native landscape: the south of France.


# VERSION FRANÇAISE

*Paysage est un caractère sans-sérif d'inspiration humaniste. Il se caractérise par un g distinctif et un rythme régulier. Paysage est dessiné pour donner un sentiment de simplicité et de quiétude au lecteur. Son design est adapté à la composition de textes courts ou longs, légendes ou annotations. Paysage s’inspire d’Antique Olive de Roger Excoffon, mais est aussi imprégné d'autres classiques comme le Rotis Sans d'Otl Aicher ou le Verdana de Matthew Carter. Le set de caractères fournit un support linguistique pour la plupart des langues européennes basées sur le latin. Paysage est une version re-dessinée, et prolongée du Garcia Regular, un caractère démarré en 2016 (plus d’infos sur l’histoire de Paysage ici).*

## Histoire de Paysage

Ce caractère est avant tout un exercice de style et s’est vu évoluer avec ma pratique : je l’ai débuté en fin 2016 sous le nom Garcia, alors que je commençais à entreprendre la création typographique. Garcia Regular avait alors beaucoup de défauts de jeunesse mais avait déjà une certaine personnalité avec un g caractéristique.

Entre temps, il a connu quelques utilisations, surtout dans le milieu de l’édition et du fanzine, ce qui m’a poussé à le mettre à jour en 2018 avec quelques améliorations mineures.

En 2019, j’ai développé une version avec plusieurs chasses pour le magazine Paysageur, qui en a fait usage. Cette version, plus extravagante, est restée confidentielle, n’étant pas encore convaincu de sa forme pour la rendre publique. Cette expérience m’a pourtant aidé a clarifier le dessin de Garcia. J’ai pu ainsi explorer de nouvelles pistes et mieux cerner ce que je souhaitais faire pour l’améliorer.

En 2020, la fonderie Tunera a commencé à utiliser le caractère, ce qui m’a motivé à développer au moins une autre graisse à partir du caractère d’origine. J’ai proposé à la fonderie de diffuser le caractère pour lui donner plus de visibilité et j’ai décidé d’en proposer une version plus sobre et adaptée à l’interface de leur site.  J’ai alors renommé le caractère Paysage. D’abord en regard de son histoire avec la revue Paysageur, mais aussi pour faire un clin d’œil à mon paysage natal : le sud de la France.

# Informations

## Crédits

Thanks to Ariel Martín Pérez et Jérémy Landes for your valuable advice.
Thanks to [Asile](http://www.instagram.com/asile.illu) for the specimen's illustrations.

*Merci à Ariel Martín Pérez et Jérémy Landes pour vos bons conseils.
Merci à [Asile](http://www.instagram.com/asile.illu) pour les illustrations du specimen.*


## License

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is available [here](http://www.tunera.xyz/licenses/sil-open-font-license-1.1/)

*Cette police est sous licence SIL Open Font License.
Cette licence est disponible [ici](http://www.tunera.xyz/licenses/sil-open-font-license-1.1/)*

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/raphaelbastide/Unified-Font-Repository

*Ce dépôt est basé sur la structure de l'Unified Font Repository.
un moyen standard d'organiser les fichiers sources des projets de polices.
Pour en savoir plus, voir
https://github.com/raphaelbastide/Unified-Font-Repository*
