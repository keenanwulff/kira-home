import Path from 'path';
import fs from 'fs';
import Handlebars from 'handlebars';
import Processor, { Asciidoctor } from 'asciidoctor';
// @ts-ignore
import * as kroki from 'asciidoctor-kroki';
import { renderHTML, renderTex, getDefaultResume } from '@kirawulff/kira-resume';
import feather from 'feather-icons';
import { FeatherIconNames } from 'feather-icons';

class CommonData {
    title: string;
    style: string;
    baseUrl: string;
    lastBuildDate: Date
    // TODO probably shouldn't be common
    resume: string;

    constructor() {
        this.title = 'Kira Home';
        this.style = 'default.css';
        this.baseUrl = 'https://kirawulff.space';
        this.lastBuildDate = new Date();
        this.resume = renderHTML();
    }

}
const commonData = new CommonData();
type PageMetadata = {

    title: string;
    summary: string;
    slug: string;
    headerImage: string;
    //constructor(title: string, summary: string, slug: string, headerImage?: string) {
    //    this.title = title;
    //    this.summary = summary;
    //    this.slug = slug;
    //    this.headerImage = (headerImage) ? headerImage : "";
    //}
}
type Page = {
    pageMetadata: PageMetadata;
    document: string;
}
type Post = {
    page: Page
    created: Date;
    updated: Date;
}

type Directories = {
    source: string;
    dist: string;
    //constructor(source: string, dist: string) {
    //    this.source = source;
    //    this.dist = dist;
    //}
}
type Paths = {
    layout: string;
    pages: string;
    pieces: string;
    posts: string;
    xml: string;
    //constructor(layout: string, pages: string, pieces: string, posts: string, xml: string) {
    //    this.layout = layout;
    //    this.pages = pages;
    //    this.pieces = pieces;
    //    this.posts = posts;
    //    this.xml = xml;
    //}
}

const paths: Paths = {
    layout: 'layouts/',
    pages: 'pages/',
    pieces: 'pieces/',
    posts: 'posts/',
    xml: 'xml/'
};


interface OutputFiles {
    [index: string]: string;
}

//type AsciidocPage = {
//    page: string;
//
//
//};
/** 
 * An annoying thing is that the metadata returned by Asciidoctor
 * gives us a date, but only the last modified date, which means the
 * remaining way to determine is through the filename itself
 */
const parseDate = (filename: string) => {
    const match = filename.match(/\d{4}-[01]\d-[0-3]\d/);
    return (match && match.length > 0) ? match[0] : null;


}

// lol we're doing a singleton because I feel like it
class AsciidocRenderer {
    private static _instance: AsciidocRenderer;
    private asciidoctor: Asciidoctor;
    private constructor() {
        this.asciidoctor = Processor.default();
    }
    public static get instance() {
        return this._instance || (this._instance = new this());
    }

    load(sourceDir: string, filename: string): Asciidoctor.Document {
        let file = this.asciidoctor.loadFile(sourceDir + filename, {
            'to_file': false,
            'safe': 'server',
            'attributes': {
                'source-highlighter': 'rouge',
                'showtitle': false,
                'base-url': commonData.baseUrl,
                'kroki-fetch-diagram': true
            }
        });
        return file;
    }

    renderPage(sourceDir: string, outBase: string, filename: string): Page {
        const file = AsciidocRenderer._instance.load(sourceDir, filename);
        const document = file.convert();
        const attributes = file.getAttributes()
        return {
            pageMetadata: {
                title: attributes['doctitle'],
                summary: attributes['description'],
                slug: outBase + attributes['docname'] + '.html',
                headerImage: attributes['header-image'],
            },
            document: document
        };
    }
    renderPost(sourceDir: string, outBase: string, filename: string): Post {
        const file = AsciidocRenderer._instance.load(sourceDir, filename);
        const document = file.convert();
        const attributes = file.getAttributes()
        const docname = attributes['docname']
        // This may return a null value, but let's be real babe,
        // this is Javascript, everything may return a null
        // I'm not currently in the business of studiously checking errors
        // (hell I don't even have tests)
        const parsedDate = parseDate(docname);
        if (parsedDate == null) throw new Error("Cannot parse date");
        const dateParsedDate = new Date(parsedDate);
        return {
            page: {
                pageMetadata: {
                    title: attributes['doctitle'],
                    summary: attributes['description'],
                    slug: outBase + docname + '.html',
                    headerImage: attributes['header-image'],
                },
                document: document
            },
            created: dateParsedDate,
            updated: new Date(attributes['docdatetime']),
        };

    }
}


const buildPosts = (dirs: Directories) => {
    let posts: Post[] = [];
    let asciidoctor = Processor.default();
    kroki.register(asciidoctor.Extensions);

    let postsPath = dirs.source + paths.posts;
    fs.readdirSync(postsPath)
        .filter(filename => filename.endsWith('.adoc'))
        .forEach(filename => {
            posts.push(AsciidocRenderer.instance.renderPost(postsPath, paths.posts, filename));
        });

    posts.sort((a, b) => {
        return b.created.getTime() - a.created.getTime();
    });
    return posts;

}

const buildPieces = (dirs: Directories) => {
    const piecesPath = dirs.source + paths.pieces
    fs.readdirSync(piecesPath)
        .filter(filename => filename.endsWith('.hbs'))
        .forEach(filename => {
            let file = fs.readFileSync(piecesPath + filename, 'utf8').toString();
            Handlebars.registerPartial(Path.parse(Path.parse(filename).name.toString()).name, file);
        });
}

const buildXML = (
    dirs: Directories,
    posts: Post[]
) => {
    let output: OutputFiles = {}
    const xmlPath = dirs.source + paths.xml;
    fs.readdirSync(xmlPath)
        .filter(filename => filename.endsWith('.hbs'))
        .forEach(filename => {

            let file = fs.readFileSync(xmlPath + filename, 'utf8').toString();
            let page = Handlebars.compile(file);
            const bodyData = {
                'common': commonData,
                'posts': posts
            };
            const body = page(bodyData);

            output[dirs.dist + Path.parse(filename).name] = body;

        });
    return output;

}


const buildPages = (
    dirs: Directories,
    layout: Handlebars.TemplateDelegate,
    posts: Post[]
) => {
    let output: OutputFiles = {}
    const pagesPath = dirs.source + paths.pages;
    fs.readdirSync(pagesPath)
        .filter(filename => filename.endsWith('.hbs') || filename.endsWith('.adoc'))
        .forEach(filename => {
            let file;
            let metadata;
            if (filename.endsWith('.adoc')) {
                const page = AsciidocRenderer.instance.renderPage(pagesPath, "", filename);
                file = page.document;
                metadata = page.pageMetadata;
            } else {
                file = fs.readFileSync(pagesPath + filename, 'utf8').toString();
            }

            let pageTitle = "";
            let pageSummary = ""
            Handlebars.unregisterHelper('setMetadata');
            Handlebars.registerHelper('setMetadata', (title: string, summary: string) => {
                pageTitle = title;
                pageSummary = summary;
            });
            let page = Handlebars.compile(file);
            // I don't much like this but we do what we gotta
            if (!filename.endsWith('.adoc')) {
                metadata = { title: pageTitle, summary: pageSummary, slug: Path.parse(filename).name };
            }
            const bodyData = {
                'common': commonData,
                'posts': posts
            };
            const body = page(bodyData);
            const data = {
                'common': commonData,
                'body': body,
                'pageMetadata': metadata
            };
            if (metadata === undefined) throw new Error("Page metadata is undefined");
            output[dirs.dist + metadata.slug] = layout(data);

        });
    return output;
}

// The reason Blog pages have to be separated differently from 
// normal pages is because their page slugs will have to be auto generated
const buildBlogPages = (
    dirs: Directories,
    layout: Handlebars.TemplateDelegate,
    posts: Post[]
) => {
    let output: OutputFiles = {};
    let file = fs.readFileSync(dirs.source + paths.layout + 'blog-post.html.hbs', 'utf8').toString();
    let page = Handlebars.compile(file);
    posts.forEach((post) => {
        const postData = {
            'post': post,
            'common': commonData,
        };
        const layoutData = {
            'common': commonData,
            'body': page(postData),
            'pageMetadata': post.page.pageMetadata,
        };
        output[dirs.dist + post.page.pageMetadata.slug] = layout(layoutData);
    });
    return output;


}


const buildHTML = (dirs: Directories) => {
    Handlebars.registerHelper('date', (datetime) => {
        return datetime.toDateString();
    });
    Handlebars.registerHelper('rfc5322', (datetime: Date) => {
        // note that I am treating the following as equivalent, at least for date formats:
        //   rfc822
        //   rfc2822
        //   rfc5322
        //   rfc6854
        //   rfc7231
        // I have no idea if it's correct to do so, but mdn specifies toUTCString as abiding by rfc7231
        return datetime.toUTCString();
    });
    Handlebars.registerHelper('year', (datetime: Date) => {
        return datetime.getFullYear();
    });
    Handlebars.registerHelper('icon', (iconName: string) => {
        return feather.icons[iconName.toLowerCase() as FeatherIconNames].toSvg({ width: 16, height: 16 });
    });
    buildPieces(dirs);
    const posts = buildPosts(dirs)
    const layoutFile = fs.readFileSync(dirs.source + paths['layout'] + 'main.html.hbs').toString();
    const layout = Handlebars.compile(layoutFile);

    const pages = buildPages(dirs, layout, posts);
    const xml = buildXML(dirs, posts);
    const blogPages = buildBlogPages(dirs, layout, posts);
    return { ...pages, ...xml, ...blogPages };
}


const main = async (dirs: Directories, renderTexOutput: boolean) => {
    if (!fs.existsSync('dist/posts')) {
        fs.mkdirSync('dist/posts');
    }

    const output = buildHTML(dirs);
    Object.keys(output).forEach(page => {
        fs.writeFile(page, output[page], (err) => {
            if (err) {
                console.error(err);
                return;
            }
        });
    });
    fs.writeFile('dist/kira-resume.json', getDefaultResume(), (err) => {
        if (err) {
            console.error(err);
            return;
        }
    });
    if (renderTexOutput) {
        renderTex((err, path) => {
            if (err) {
                console.error(err);
            }
            fs.copyFile(path as URL, 'dist/kira-resume.pdf', (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
            });

        });
    }
}

main({ source: 'src/', dist: 'dist/' }, true);
