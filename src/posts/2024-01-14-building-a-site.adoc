= Building A Website in the Year 2024
:description: How I built this
:astro: https://astro.build/
:astro-tutorial: https://docs.astro.build/en/tutorial/0-introduction/
:ghost: https://ghost.org/

Over the last few months, I've been working on and off to build this, a simple personal website and blog which you are reading this through right now.
This is somewhat of a culmination of my early time unemployed, although parts of this started long before.
It's a lot of what I want from a personal project: reasonably scoped, allowing for creativity in design, and utilizing fundamental technologies that previously were partial gaps in my knowledge.

IMPORTANT: I'm not an expert in frontend web development, so pleace don't take me as an authority.
I try to follow good practices, and I take inspiration from projects I consider to have authority on best practices.
To be quite frank, web development in general is awash in questionable practices and code, and I'm sure I fell for some of those (I mean hell, prior to this, my primary frontend experience was in Angular).

== Design Goals

Primarily, I wanted to meet three adjectives: functional, personal, and minimal.
These are all vague and extremely subjective, so it's worth going through these in more depth

=== Functional

I wanted a basic website and blog where I can write about interests, keep my resume, and redirect folks to other presences on my web if needed.
I also want this to be mine — while I'm not out here hosting this on my own server, there's a certain romanticism in the old web model of having a space that is completely your own.
In the last 15 years or so, I feel like people on the internet have been sold the idea of giving over all their thoughts and data to large companies to be distributed, repackaged, and mined for advertising data.

Having your own website isn't immune to that, the unknowable gods guiding Google's PageRank algorithm still index this site.
But I don't need reach or network effects, I just want a place on the net, and this is what I've got.

So functional guidelines (not calling these requirements because I wrote them after the fact, which in my mind is perfectly fine for a personal project):

. Must have at least three pages: a homepage, a blog, and a resume
. Must allow for writing blog posts in a markup language that doesn't suck to write in (sorry HTML, you're beautiful and perfect in every way)
** Should have the ability to richly display code and math
. Must be controlled in source control
. Must serve a resume in both inline HTML and as a PDF
** A change in resume data must update both the HTML and the PDF when committed into source control
. Should have an RSS feed
. Costs should be kept below $10 a month (please hire me)

These shouldn't need much editorial, they're all mostly straightforward, but I do want to define some terms for those reading this that may not already be versed in weird developer lingo

One other thing I want to add here is that the resume requirements are something I haven't personally seen in other projects.
It ends up being an interesting problem and one I think I found a good solution for, so I'll leave that for a separate blog post.

=== Personal

Here, I just want something that looks like it has a personal identity.
This is another thing I think we've lost in the social era: there's just no way to add design to a social network profile, and even in the areas of the web outside the bounds controlled by social media companies, websites are becoming https://www.theverge.com/c/23998379/google-search-seo-algorithm-webpage-optimization:[increasingly homogeneous] due to the unspoken rules surrounding search engine optimization.
I'm not here to create a GeoCities website (I do need a modicum of respectability in my web design; hire me), but I do want something that is at least unique.

=== Minimal

I just want my website to really communicate the vibe of a beige-walled room with an air mattress in its center.

Really, what I'm getting at here is simplicity in the tech stack.
I love a React app that hooks back to a Redis cache, a fancy no-SQL database, and a Kubernetes cluster of multiple bespoke microservices as much as the next girl, but I just need a blog here.

Some of this is just picking my battles; I don't need to configure an EC2 instance if Gitlab can serve static HTML just fine.
I have no requirement for paywalls or user authentication, so I don't need a database or a management platform that something like Ghost would provide.
I also have no ambition to track you as you move throughout the web, and don't need session persistence, so I'm good without having to deal with cookies.

Beyond that, I decided early on that as a personal project, I wanted to learn things.
I've never made a website that is just a bundle of HTML and CSS before, and I wanted to understand both how to do that, and what the bounds of the approach is.
I've worked with static site generators before, and they're great at what they do, but sometimes it's good to unwrap a few layers of abstraction to learn how those things are actually preparing the static sites.

As an addendum, if learning about how to generate a site from first (ish) principles isn't a primary goal, there are tools that make this process a lot more friendly at a bunch of different levels of abstraction.
See <<design-alternatives>> below for a few ideas.

Static site:: A website which does not need to be modified in order to be delivered to a web browser.
This means that a purpose-built server isn't necessary; a server only needs to give you files exactly as they're stored.
Static site generator:: A framework which performs static site construction, allowing a user to focus more on content than process.

I did end up compromising for the sake of functionality here — I had originally wanted to do pure HTML and no client side JavaScript, but I ended up using a templating library for my HTML and the most popular way to do code highlighting involves a client side library.

== Tech Stack

=== TypeScript and Node

JavaScript, being the only first class language for the web, is mostly non-negotiable in frontend, but hardly a forgone conclusion for preprocessing or backend work

Excluding client-side JavaScript, there's no need to actually generate a static site in JavaScript, because the code used to lay out a website doesn't necessarily have to be included within a website itself.
Some of the more popular static site generators have used Ruby, Go, Python, or Rust.

My thought process, then, was basically "when in Rome."
I'm doing web development, might as well use the web development language.
If you're running JavaScript outside the browser, you need Node as the runtime, so that comes along as well.

I only know what I know though, and I'm an acolyte of statically-typed languages, so I quickly swapped in TypeScript
After writing a bit of code in plain JavaScript, I got frustrated at the absurd leinency of JavaScript's dynamic typing, so TypeScript quickly followed.

=== HTML

HTML almost doesn't merit inclusion in here, because functionally there's no other option.
My original strategy was just to write HTML and send it, but there is no reasonable way to avoid an slew of copy-paste errors if you do.
If you design an element that should be common to multiple pages, such as a header, there's no way to share that across pages, so you would have to have a copy in every single HTML file, you distribute.

If you dig into the source of any static site generator, there's likely to be some sort of templating library involved.

=== Handlebars

If a static site generator is written in JavaScript, Handlebars seems to be the go-to templating library.
It does a pretty good job of getting out of the way, but has useful features, including registering re-usable chunks, called partials, and the ability to write custom helper functions when something needs to be done involving logic more complex than a templating system can handle.

=== CSS

There exist augmentations to CSS like SASS, but here I didn't think that was necessary.
I only actually use one CSS file to style the whole site (there is another that's required by the code highlighting, but I'm using a premade theme there), so anything that goes beyond basic styling is out of scope here.

=== Git and Gitlab

Git is industry-standard source control, even if I think it's more user-hostile than it strictly needs to be.
Having managed complex projects with multiple contributors, I'm very proficient to it by now, so I don't have much to complain about.

Modern source control management platforms bundle in a lot of things that aren't just about source control management, including CI/CD pipelines which can compile and distribute your code and static site hosting, and the default here would be Github.
I even could go with something like Forgejo, and in the months since creating this site, I have stood up a self-hosted version of that service on my new home server, although there's a lot of logic to using a public, reliable service than the one precariously running on the computer on the other side of this wall.
I ended up choosing Gitlab because it's the tool I know.

For all the faults I find with the project management of Gitlab (it has a lot of features, but many are incomplete or inadequate), I really like the straightforwardness of Gitlab's CI/CD syntax.
You give it a Docker image, you give it a set of actions, and that's about it.
If you want to host a static site using its Pages feature, you just dump the stuff you want to distribute and tell Gitlab where it should expect to find it.

I'm also using Gitlab's multi-project pipelines and NPM package hosting for the resume side, which I'll get into in that post

== Design Alternatives

There are so many ways I could have approached this that are different from what I ended up with -- a blog has been a solved problem on the web since before the GeoCities days -- but I want to walk through a couple of solutions that I find interesting.

I didn't know of the {astro}:[Astro framework] until months after I built this site, but I think it's the first time I've fully meshed with a web framework.
The way a project is typically structured in Astro is functionally very similar to the design space I was in, but the tool itself has thought through the things that I wasn't able to: 

. How do you functionally mix content that are persistent, discrete pages with blog posts and other things that need a dynamic routing structure without having to write bespoke preprocessors?
. How do you add page-specific preprocessing in a way that is localized to the page itself?
. How can you evolve a static site to one that does need dynamic routing?

There are a lot of ways to build complex applications online today, but if what you want to build is foremost a website, Astro seems like a great place to start.
They have a great {astro-tutorial}:[tutorial] which will get you to a basic blog in about a day.

There are also alternatives that don't require the technical knowledge, and come with fully-featured CMS solutions, payment solutions, and even hosting platforms.
Given the community schisms that have lit up around Wordpress and Substack recently, {ghost}:[Ghost] is the service I've seen a bunch of bloggers switch to recently, and I don't know of anyone I follow that has been disappointed with it.
You can start with a fully hosted solution or run it standalone, it has payment, built-in themes, and analytics, and it's still open source


== Project Structure

We begin, of course, with a `src/` folder and a `dist/` folder.
Everything that does not need preprocessing goes in the `dist/` (for me, that's the favicon images, the CSS, the code rendering JavaScript, a `robots.txt` and the fonts) folder, which leaves the HTML templates and Asciidoc files in the `src/` folder.
When the preprocessing happens, the output files also get thrown into the `dist/` directory.
Were I to redo this, I'd probably stick even the static files in the src folder for the simple reason that it's nice to be able to completely delete your build ephemera (as well as stick it in the gitignore), and having files in it that can't be reproduced by the build process means you can accidentally delete things you'd rather not.

After everything is built, the `dist/` directory becomes the static files served by Gitlab.

In the `src/` directory, there's a relatively simple structure:


[source]
----
src
├── layouts
│   ├── blog-post.html.hbs
│   └── main.html.hbs
├── pages
│   ├── blog.html.hbs
│   ├── index.html.hbs
│   └── resume.html.hbs
├── pieces
│   ├── entry.html.hbs
│   ├── footer.html.hbs
│   └── header.html.hbs
├── posts
│   ├── 2024-01-14-blog-1.adoc
│   ├── 2024-06-23-blog-2.adoc
│   └── 2024-07-02-blog-3.adoc
└── xml
    └── rss.xml.hbs
----

The `.hbs` syntax indicates a Handlebars template, but doesn't necessarily indicate a full page; the only files here which end up with a route to them on the website are within the `pages/` and `posts/` directories.
The other two subdirectories with HTML Handlebars templates, `layouts/` and `pieces/` represent things needed to make pages.

== Composable design with templates

Start with `pieces/`.
Here's an example:

.src/layouts/main.html.hbs
[source, html]
----
<header id="header-box">
    <div id="header">
	<div class="nav-bar-container">
    <a href="{{ common.baseUrl }}/index.html" rel="home" id="home">
      <div class="page-heading">Kira Wulff</div>
    </a>
    <nav>
      <a href="{{ common.baseUrl }}/blog.html" id="blog">
        <div class="nav-text">blog</div>
      </a>
      <a href="{{ common.baseUrl }}/resume.html" id="resume">
        <div class="nav-text">resume</div>
      </a>
    </nav>
	</div>
  </div>
  <div id="header-rect-1"></div>
  <div id="header-rect-2"></div>
</header>
----

This is the header bar at the top of each page in the website.
This is a composable piece of a website, and if we were writing in raw HTML, we'd have to copy this into every page in the website.
What Handlebars accomplishes here is the copy/paste, which in turn frees me from having to keep it consistent across multiple files.

The files in the `layouts/` directory inverts the metaphor; instead of being small composable things that can be put into pages, layouts are the structure into which page content is laid.
Here's the main page layout:

.src/layouts/main.html.hbs
[source, html]
----
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="{{common.baseUrl}}/favicon.svg" type="image/svg+xml">
  <link rel="stylesheet" type="text/css" href="{{common.baseUrl}}/css/tokyo-night-dark.min.css">
  <script type="text/javascript" src="{{common.baseUrl}}/scripts/highlight.min.js"></script>
  <script>hljs.highlightAll();</script>
  <link rel="stylesheet" href="{{common.baseUrl}}/css/{{common.style}}">
  <title> {{ common.title }} </title>
</head>
<body>
  {{> header}}
  <div class="content">
    {{{ body }}}
  </div>
  {{> footer}}
</body>
</html>
----

The headmatter should be recognizable to anyone who's ever written a website: it has a link to the stylesheet (I'm only using one global stylesheet), a link to the single script I have going to the frontend, the favicon, and the title.
The body, meanwhile, is extremely simple: I'm using the header and footer from the pieces directory, and then simply telling the templating engine to put whatever unique content is needed for the individual webpage there as the body.

The body content then, is whatever unique content needs to go in the page, and these are in either `pages/` or `posts/`.
The `pages/` are the simpler of the two; they're literally just HTML content that is lifted and given to the layout engine.
`posts/` are in our lightweight markup language, Asciidoc, and thus must be rendered into HTML before being given as data to the layout.
Importantly, we have to run this through another layout, the blog post layout, to stick the metadata onto the post:

.src/layouts/blog-post.html.hbs
[source, html]
----
<h1>{{{ post.title }}}</h1>
<p class="datetime"> {{{ date post.created }}} updated: {{{ date post.updated }}}  </p>
<p class="post-summary"> {{{ post.summary }}} </p>
<hr>
{{{ post.post }}}
----

All of this logic is handled in `index.ts`


== The business logic

All data, including things like the body content that is interpolated into the page, is given in a JavaScript dictionary.
The basic flow for using Handlerbars for this purpose is this:

.index.ts
[source, typescript]
----
// Read file (we're using Node here)
let file = fs.readFileSync(pagesPath + filename, 'utf8').toString();
// Compile the page into a function object
let page = Handlebars.compile(file);
// The common data includes things like base URL and stylesheets
const bodyData = {
  'common': common
  ...
};
// Do all the templating stuff on the body
const body = page(bodyData);
const data = {
  'common': common
  'body': body,
  ...
};
// The layout here is passed in to the function (that's just how I implemented), although it's a function object of the same type as page
output = layout(data);

----

Basically, we template the body, then give the body as data to the layout when templating the layout.
The things in the `pieces/` directory are registered to Handlebars as Partials, which is a template reuse system that Handlebars provides.
We can treat them as global to the instantiation of Handlebars used.

Asciidoc rendering for posts uses Asciidoctor.js:

.index.ts
[source, typescript]
----

let file = asciidoctor.loadFile(postsPath + filename);
let postHtml = file.convert( {  to_file: false, attributes: {
  'source-highlighter': 'rouge',
  'showtitle': true
}});
// Attributes are metadata that Asciidoc knows about. We need some of them
const attributes = file.getAttributes()
const docname = attributes['docname'];
// Since my convention is to have a post date as part of the file name of every post, I can parse that out and use as the date.
const parsedDate = parseDate(docname);

if (parsedDate) {
    posts.push(new Post(
        attributes['doctitle'],
        new Date(parsedDate), /* post date */
        new Date(attributes['docdatetime']), /* date updated */
        attributes['description'],
        postHtml,
        paths.posts + attributes['docname'] + '.html'
    ));
}
----

This is all composed into a few steps:

.index.ts
[source, typescript]
----
const buildHTML = (dirs: Directories, paths: Paths, commonData: CommonData) => {
    Handlebars.registerHelper('date', (datetime) => {
        return datetime.toDateString();
    });

    // ...

    const posts = buildPosts(dirs, paths)
    const layoutFile = fs.readFileSync(dirs.source + paths['layout'] + 'main.html.hbs').toString();
    ...
    buildPieces(dirs, paths);
    const layout = Handlebars.compile(layoutFile);

    const pages = buildPages(commonData, dirs, paths, layout, posts);
    const xml = buildXML(commonData, dirs, paths, posts);
    const blogPages =  buildBlogPages(commonData, dirs, paths, layout, posts);
    return {...pages, ...xml, ...blogPages};
}
----

There are a few things I haven't talked to here.
The first are the helpers, of which I've removed all but one for brevity.
These are functions that you can register in code then use in the template itself; the one shown simply formats a date string.

The second thing is the `buildXML` function, which I use for the RSS feed.
An RSS feed is basically one long document containing a bunch of individual items, in this case blog posts.
Except for being XML instead of HTML, I use the same templating techniques for that, and its format can be seen at `src/rss`.

Everything else is straightforward: register the pieces, make the posts, then build the pages and RSS.
It all gets returned to a `main` function that writes everything back out to disk.

'''

That covers almost all of the technical details from a high level view.
There is the slight issue of the resume page, but I'll have details on that later
